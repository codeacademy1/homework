let globalData;
// hide or show #jobTitleInput
document.getElementById("jobInput").onchange = () => {
  let jobTitleLabel = document.getElementById("jobTitleLabel");
  let jobTitle = document.getElementById("jobTitleInput");
  if (jobTitle.hidden) {
    jobTitle.value = "";
  } else {
    jobTitle.value = "Unemployed";
  }
  jobTitleLabel.hidden = !jobTitleLabel.hidden;
  jobTitle.hidden = !jobTitle.hidden;
};

/**
 * Ovaa Funkcija zima parametri od URLSearchParams objekt i
 * gi dodava podatocite od form vo globalData
 *
 * @param {URLSearchParams} searchParams: parametri od URL
 */
function createGlobalDataObject(searchParams) {
  const name = searchParams.get("name");
  const age = searchParams.get("age");
  const job = searchParams.get("job");
  const gender = searchParams.get("gender");
  const livesInSkopje =
    searchParams.get("livesInSkopje") !== "on" ? false : true;
  globalData = { name, age, job, gender, livesInSkopje }; // ako nekoj taka bez vrska dodava se kje bide null null null
}



/**
 * Ovaa funkcija kreira elementi sho gi prikazhuvat
 * podatocite vneseni vo prethodnata forma
 *
 */
function createDataSection() {
  const dataSection = document.getElementById("data-section");
  dataSection.hidden = false;
  const { name, age, job, gender, livesInSkopje } = globalData;
  dataSection.innerHTML = `<fieldset>
    <legend>Data you entered</legend>
    <div class="input-set">
      <label>Name: ${name}</label>
    </div>
    <div class="input-set">
      <label>Age: ${age}</label>
    </div>
    <div class="input-set">
      <label>Job: ${job}</label>
    </div>
    <div class="input-set">
      <label>Gender: ${gender}</label>
    </div>
    <div class="input-set">
      <label>Lives in Skopje: ${livesInSkopje}</label>
    </div>
  </fieldset>`; // zamisluvam si deka react vaka go crta DOM
}

// Ako e napraven form submit kreiraj objekt u document i skij ja form sekcijata
window.addEventListener("load", () => {
  const queryString = window.location.search;
  if (queryString) {  // ako ima query string smeni go izgledo na stranata
    document.globalData = createGlobalDataObject(
      new URLSearchParams(queryString)
    );
    document.getElementById("form-section").hidden = true;
    createDataSection();
  }
});

//                            Od tuka pochnuva domashnoto

// Const poradi toa sho sa const osven age i job
const fullName = "Metodi Gicev";
const age = 34;
const job = "Unemployed";
const gender = "Male";
const livesInSkopje = false;

const homeworkData = { fullName, age, job, gender, livesInSkopje };
console.log(homeworkData)

/**
 * Kliknesh i odma alert
 *
 */
function domashnoClick() {
  alert(`${fullName}, ${age}, ${job}, ${gender}, ${livesInSkopje}`)
}
