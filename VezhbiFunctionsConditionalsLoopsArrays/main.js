"use strict";
// https://javascript.plainenglish.io/javascript-optimizations-tail-call-optimization-tco-471b4f8e4f37
// za toa 'use strict';

const nonIntErr = Error("Array contains non integer members");

const convertToNumbers = (arr) => arr.flat(10).map((x) => parseInt(x));

const includesNaN = (arr) => arr.includes(NaN);

// Nishto ne garantiram deka kje rabote kako sho treba

// version 1
/**
 * Presmetuva suma na elementi vo array
 *
 * @param {Array} arr
 * @return {Error | Number} 
 */
function evalSumOfArray(arr) {
  arr = convertToNumbers(arr);
  try {
    if (includesNaN(arr)) throw nonIntErr;
    return eval(
      arr.join("+") // string so site elementi povrzani so "+". [1,2] = "1+2"
    );
  } catch (err) {    
    return err;
  }
}

console.log(evalSumOfArray([1, 2, 3, 12345, 56789, 6543210, "c"]));
console.log(evalSumOfArray([1, 2, 3, 12345, 56789, 6543210]));

// version 2
// isto oneliner
/**
 * Presmetuva suma na elementi vo array
 *
 * @param {Array} arr
 * @return {Number}
 */
function reduceSumOfArray(arr) {
  arr = convertToNumbers(arr);
  try {
    if (includesNaN(arr)) throw nonIntErr;
    return arr.reduce((x, y) => x + y); // suma
  } catch (err) {
    return err;
  }
}

console.log(reduceSumOfArray([1, 2, 3, 12345, 56789, 6543210, "c"]));
console.log(reduceSumOfArray([1, 2, 3, 56789, 6543210]));

// version 3
// recursion - potrazhi na google
/**
 * Rekurziven SUM
 *
 * @param {Array} [first, ...next]
 * @param {Number} [sum=0]
 * @return {Number}
 */
function recursiveSumOfArray([first, ...next], sum = 0) {
  // pretpostavuvame deka usero kje dade array kako prv argument
  // inaku nema poenta da e rekurzivna funkcijata ako ne e tail-call optimized
  if (!next.length) {
    return sum + first;
  } else {
    return recursiveSumOfArray(next, sum + first);
  }
}

// primer
const niza = convertToNumbers([1, 2, 3, 12345, 56789, 6543210, "14c"]);
try {
  if (includesNaN(niza)) throw nonIntErr;
  console.log(recursiveSumOfArray(niza));
} catch (err) {
  console.error(err);
}

// Sum so for loop
/**
 * SUM funkcija so for loop
 *
 * deokumentacijata ja pishuva samo zaradi code completition
 * na parametrite
 *
 * @param {Array} arr
 */
function sum(arr) {
  let sum = 0; // pa kje recham ovie ne se zdravi naviki
  try {
    for (let x of convertToNumbers(arr)) {
      if (isNaN(x)) throw nonIntErr;
      sum += x;
    }
    return sum;
  } catch (e) {
    console.error(e);
    return null;
  }
}

// magichen sum
function magicSum(arr) {
  const gensum = (function* (a) {
    yield* convertToNumbers(a);
  })(arr);
  let sum = 0;
  try {
    for (let x of gensum) {
      if (isNaN(x)) throw nonIntErr;
      sum += x;
    }
    return sum;
  } catch (err) {
    console.error(err);
    return null;
  }
}

console.log(magicSum([0, 2, 5, 8]));
console.log(magicSum([0, 2, 5, 8, "9"]));
console.log(magicSum([0, 2, 5, "8", "16", 35, 4]));
console.log(magicSum([0, 2, 5, 8, "15a"]));

// duplikati
/**
 * Najlesen i mislam najkoristen nachin za vrakjanje duplikati
 *
 * @param {Array} [first, ...next]
 * @param {Array} [dup=[]]
 * @return {Array}
 */
function duplikati([first, ...next], dup = []) {
  if (next.includes(first) && !dup.includes(first)) {
    dup.push(first);
  }
  if (!next.length) {
    return dup;
  } else {
    return duplikati(next, dup);
  }
}

/**
 * Ja se dlaboko izvinjavam za reshenieto tuka
 * ama zasho bi pishuvale nepotrebni for exp.
 *
 * @param {Array} arr
 * @return {Array} sortiran array bez duplikati
 */
function sortirajIIzvadiDuplikati(arr) {
  return [...new Set(arr)].sort();
}
