//1
const library = [
  { author: "Bill Gates", title: "The Road Ahead", libraryID: 1254 },
  { author: "Steve Jobs", title: "Walter Isaacson", libraryID: 4264 },
  { author: "Suzanne Collins", title: "Mockingjay", libraryID: 3245 },
  { author: "Metodi Gitsev", title: "Bla Bla BLa", libraryID: 5534 },
];

function sortByTitleAZ(coll) {
  return coll.sort((x, y) => (x.title < y.title ? -1 : 1));
}

console.log(sortByTitleAZ(library));

function getAuthors(coll) {
  return coll.map(({ author }) => author);
}

console.log(getAuthors(library));

function filterById(coll) {
  return (id) => coll.filter(({ libraryID }) => libraryID === id);
}

console.log(filterById(library)(1254));

//2
class Car extends Object {
  /**
   * Creates an instance of Car ili ne.
   * @param {String} brand
   * @param {Number} year - tekovna ili pomala godina
   * @param {Number} horsepower - pomalku od 1002
   * @memberof Car
   */
  constructor(brand, year, horsepower) {
    super();
    const currentYear = new Date().getFullYear();
    this.brand = brand;
    this.year = year < currentYear ? year : currentYear;
    if (horsepower >= 1001) {
      throw Error("Tie konji ne se sobirat u ovaa klasa.");
    }
    this.horsepower = horsepower;
  }

  /**
   * Presmetuva starost na avtomobil
   *
   * @return {Number}
   * @memberof Car
   */
  carAge() {
    return new Date().getFullYear() - this.year;
  }

  /**
   * Proveruva dali nekoj mozhe zakonski da voze takov avtomobil
   *
   * @param {Number} { name: String, age: Number }
   * @return {Boolean} - boolean
   * @memberof Car
   */
  userCanDrive({ age }) {
    return this.horsepower > 105
      ? age > 25
        ? true
        : false
      : age >= 18
      ? true
      : false;
  }

  /**
   * Ovoa ko go stavesh u string vrakja celosen opis na objekto
   *
   * @param {String} hint - typehint
   * @return {String || Object}
   * @memberof Car
   */
  [Symbol.toPrimitive](hint) {
    if (hint == "string") {
      return `${this.brand}, ${this.year} (${this.carAge()} years old), ${
        this.horsepower
      }bhp`;
    } else {
      return NaN;
    }
  }
}

const users = [
  { name: "Igor", age: 15 },
  { name: "Vasili", age: 22 },
  { name: "Ivan", age: 31 },
  { name: "Jovan", age: 25 },
  { name: "Goran", age: 18 },
];
const cars = [
  new Car("Fiat", 2001, 95),
  new Car("Audi", 2019, 300),
  new Car("Reno", 2019, 104),
];

for (const user of users) {
  for (const car of cars) {
    if (car.userCanDrive(user)) console.log(`${user.name} can drive ${car}.`);
  }
}
