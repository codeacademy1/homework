'use strict';
// Za tail-call optimizacija poradi toa sho ne optimizira po default
function equalityBetweenThreeVariables(a, b, c) {
  [a, b, c] = [a, b, c].map((x) => parseInt(x));
  if (a === b || a === c || b === c) {
    return true;
  } else {
    return false;
  }
  /*
    a mozheshe da bide samo
    return a === b || a === c || b === c;
  */
}

function bla(a,b,c) {
  [a, b, c] = [a, b, c].map((x) => parseInt(x));
  return a === b || a === c || b === c;
}



console.log(equalityBetweenThreeVariables(1, 2, 3));
console.log(equalityBetweenThreeVariables(1, 1, 3));
console.log(equalityBetweenThreeVariables(1, 3, 150));
console.log(equalityBetweenThreeVariables(1, 2, "1"));
console.log(equalityBetweenThreeVariables("5", "6", 6));

const meseci = {
  1: "Januari",
  2: "Februari",
  3: "Mart",
  4: "April",
  5: "Maj",
  6: "Juni",
  7: "Juli",
  8: "Avgust",
  9: "Septemvri",
  10: "Oktomvri",
  11: "Noemvri",
  12: "Dekemvri",
};

/**
 * Proveruva godina do validen input
 *
 * @return {Array} [denovi, godina]
 */
function februaryDays() {
  const year = parseInt(prompt("Vnesi godina:"));
  try {
    if (isNaN(year)) throw Error("Nevalidna godina. Probaj Povtorno");
    return [year % 4 === 0 ? 29 : 28, year];
  } catch (err) {
    alert(err.message);
    return februaryDays();
  }
}

// ovaa funkcija e nogu opasna poradi toa sho i treba nogu vreme
function daysInMonth() {
  const month = prompt("Vnesi mesec:") * 1;
  if (month === 2) {
    const [denovi, godina] = februaryDays();
    alert(`${meseci[month]} od ${godina} ima ${denovi}.`);
  } else if (
    month === 4 ||
    month === 6 ||
    month === 9 ||
    month === 11 
  ) {
    alert(`${meseci[month]} ima 30 den.`);
  } else if (month & (month <= 12)) {
    alert(`${meseci[month]} ima 31 denovi.`);
  } else {
    alert(
      "NE MOZHE! NE SE VAZHI! KJE BIDAM DOSADEN DODEKA NE NAPISHESH VALIDEN MESEC! KLIKNI OK ZA DA PRODOLZHISH!"
    );
    daysInMonth();
  }
}
