# Домашно 2

1. ~~Create a webpage with header, sidebar (nav), main part of the page, footer (add semantic elements and follow the order of the elements).~~

2. ~~Write title of the page in the header, add several page names in the sidebar and your name in the footer that is a clickable link that points to your repository.~~

3. ~~In the main part of the page create a form with fields for *Full Name*, *Email*, *Password*, *Age Group (Bellow 18, 18-64, Above 64)*, *City (Скопје, Куманово, Тетово, Велес, Штип, Прилеп, Охрид, Битола, Струмица)*, *Transport (bus, train, car, bike, bicycle)* - can be more than one, *Submit Button at the end*.~~

4. ~~Add your website in a filder called *"Forms Homework"* and upload it to your repository.~~
