/**
 * Zima array od (nadevame se) broevi i brishe eden broj
 *
 * @param {Array} arr
 * @param {Number} num
 * @return {Error | Number}
 */
function removeNumberFromArray(arr, num) {
  const err = Error("Array contains non number members."); // greshka za nebrojki
  try {
    num *= 1; // za u sekoj sluchaj
    if (isNaN(num)) throw err; // ako ne e broj
    arr = arr.map((x) => x * 1); // za u sekoj sluchaj
    if (arr.includes(num)) {
      for (let i = 0; i < arr.length; i++) {
        const broj = arr[i];
        if (isNaN(broj)) throw err; // ako ne e broj
        if (broj === num) {
          arr.splice(i, 1);
        }
      }
    }
    return arr;
  } catch (e) {
    // ako listata sodrzhe NaN posle map(x => x*1)
    return e;
  }
}

console.error(
  removeNumberFromArray([1, 2, 5, 16, 3, 12, 3545, "c", "152"], 16)
);
console.log(removeNumberFromArray([1, 2, 3, 12, 3545, "16", "152", "2"], 2));
console.log(removeNumberFromArray([1, 2, 3, 12, 3545, "16", "152"], 152));
console.log(removeNumberFromArray([1, 2, 3, 12, 3545, "16", "152"], "3545"));

/**
 * Dokumentacija radi code completition
 *
 * @param {Number} num
 * @return {Number}
 */
function countDigits(num) {
  num = String(num).split("");
  let counter = 0; // mrazam promenlivi za toa prashav dali mozhe bez loop
  while (counter < num.length) {
    counter++;
  }
  return counter;

  /*
    a mozheshe samo return String(num).split("").length
  */
}

console.log(countDigits(1));
console.log(countDigits(Number.MAX_SAFE_INTEGER));
console.log(countDigits(12344));

/**
 * Naogja go najgolemio broj u niza
 *
 * @param {Array} arr
 * @return {Number}
 */
function max(arr) {
  let xi = 0;
  for (let x of arr) {
    xi = xi > x ? xi : x;
  }
  return xi;
}

console.log(max([1, 2, 3, 15, 155, 342, 15, 19, 35]));
